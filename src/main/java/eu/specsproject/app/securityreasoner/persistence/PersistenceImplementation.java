package eu.specsproject.app.securityreasoner.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.slf4j.LoggerFactory;

import eu.specsproject.app.securityreasoner.entities.Caiq;
import eu.specsproject.app.securityreasoner.entities.CaiqDocument;
import eu.specsproject.app.securityreasoner.entities.CaiqTree;
import eu.specsproject.app.securityreasoner.entities.Judgement;
import eu.specsproject.app.securityreasoner.entities.SLACaiq;

public class PersistenceImplementation {

	private EntityManagerFactory entityManagerFactory;

	public static boolean isTest = false;
	
	public PersistenceImplementation(){
		entityManagerFactory = PersistenceEntityManager.getInstance().getEntityManagerFactory(isTest);
	}

	//-------------------------------------------------------------------------------------//
	//-------------------------- Caiq Persistence Implementation --------------------------//
	//-------------------------------------------------------------------------------------//

	public List<String> retrieveCaiqs(int start, int stop){
		EntityManager em =  entityManagerFactory.createEntityManager();

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Caiq> cq = cb.createQuery(Caiq.class);

		List<Caiq> list;

		if(stop != -1){
			list = em.createQuery(cq).setFirstResult(start).setMaxResults(stop-start).getResultList();
		}else{
			list = em.createQuery(cq).setFirstResult(start).getResultList();
		}

		List<String> listId  = new ArrayList<String>();
		for (Caiq caiq :  list){
			listId.add(new String(caiq.getId()));
		}
		
		em.close();

		return listId;

	}

	public String createCaiq(String CSP, String caiqDocument) throws IllegalArgumentException{

		if (caiqDocument==null)
			throw new IllegalArgumentException("The Caiq cannot be null");

		Caiq internalCaiq = new Caiq();
		LoggerFactory.getLogger(PersistenceImplementation.class).debug("json: "+caiqDocument);

		internalCaiq.setCSP(CSP);
		internalCaiq.setDocumentId(storeCaiqDocument(caiqDocument));

		EntityManager em =  entityManagerFactory.createEntityManager();
		EntityTransaction t = em.getTransaction();
		t.begin();
		em.persist(internalCaiq);
		t.commit();
		em.close();

		return new  String(internalCaiq.getId());

	}

	public Caiq retrieveCaiq(String id) throws IllegalArgumentException{
		EntityManager em =  entityManagerFactory.createEntityManager();

		if (id==null)
			throw new IllegalArgumentException("Caiq_Identifier cannot be null");
		Caiq caiq = em.find(Caiq.class,id);

		if (caiq==null)
			throw new IllegalArgumentException("Caiq_Identifier is not valid");

		em.close();
		
		return caiq;
	}

	public void updateCaiq(Caiq caiq){

		EntityManager em =  entityManagerFactory.createEntityManager();
		EntityTransaction t = em.getTransaction();
		t.begin();
		em.merge(caiq);
		t.commit();
		em.close();       
	}

	public String removeCaiq(String id) throws IllegalArgumentException{

		if (id==null)
			throw new IllegalArgumentException("String cannot be null");
		EntityManager em = entityManagerFactory.createEntityManager();
		Caiq caiq = em.find(Caiq.class,id);

		if (caiq==null)
			throw new IllegalArgumentException("String is not valid");

		em.getTransaction().begin();
		em.remove(caiq);
		em.getTransaction().commit();

		em.close();
		return id;
	}

	public CaiqDocument retrieveCaiqDocument(String id) throws IllegalArgumentException{

		EntityManager em =  entityManagerFactory.createEntityManager();

		if (id==null)
			throw new IllegalArgumentException("Caiq_Identifier cannot be null");
		CaiqDocument caiq = em.find(CaiqDocument.class,id);

		if (caiq==null)
			throw new IllegalArgumentException("Caiq_Identifier is not valid");

		em.close();
		return caiq;

	}

	public String storeCaiqDocument(String caiqDocument){

		CaiqDocument caiq = new CaiqDocument();
		caiq.setcaiqXmlDocument(caiqDocument);

		EntityManager em =  entityManagerFactory.createEntityManager();
		EntityTransaction t = em.getTransaction();
		t.begin();
		em.persist(caiq);
		t.commit();
		em.close();

		return caiq.getId();

	}

	public String removeCaiqDocument(String id) throws IllegalArgumentException{

		if (id==null)
			throw new IllegalArgumentException("CaiqDocumentId cannot be null");
		EntityManager em = entityManagerFactory.createEntityManager();
		CaiqDocument caiqDocument = em.find(CaiqDocument.class,id);

		if (caiqDocument==null)
			throw new IllegalArgumentException("caiqDocument is not valid");

		em.getTransaction().begin();
		em.remove(caiqDocument);
		em.getTransaction().commit();

		em.close();
		return id;		
	}


	//-------------------------------------------------------------------------------------//
	//----------------------- Judgement Persistence Implementation ------------------------//
	//-------------------------------------------------------------------------------------//


	public List<Judgement> retrieveJudgements(int start, int stop){
		EntityManager em =  entityManagerFactory.createEntityManager();

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Judgement> cq = cb.createQuery(Judgement.class);

		List<Judgement> list;

		if(stop != -1){
			list = em.createQuery(cq).setFirstResult(start).setMaxResults(stop-start).getResultList();
		}else{
			list = em.createQuery(cq).setFirstResult(start).getResultList();
		}
		
		em.close();

		return list;        
	}

	public String createJudgement(Judgement judgement) {

		EntityManager em =  entityManagerFactory.createEntityManager();
		EntityTransaction t = em.getTransaction();
		t.begin();
		em.persist(judgement);
		t.commit();
		em.close();        

		return new  String(judgement.getId());        
	}

	public Judgement retrieveJudgement(String id) throws IllegalArgumentException{
		EntityManager em =  entityManagerFactory.createEntityManager();

		if (id==null)
			throw new IllegalArgumentException("Judgement_Identifier cannot be null");
		Judgement judgement = em.find(Judgement.class,id);

		if (judgement==null)
			throw new IllegalArgumentException("Judgement_Identifier is not valid");

		em.close();
		return judgement;
	}

	public Judgement retrieveDefaultJudgement() throws IllegalAccessError{

		EntityManager em =  entityManagerFactory.createEntityManager();

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Judgement> cq = cb.createQuery(Judgement.class);

		Root<Judgement> root = cq.from(Judgement.class);
		cq.where(cb.equal(root.get("defaultJudgement"), true));
		Judgement judgement = em.createQuery(cq).getSingleResult();
		if (judgement==null)
			throw new IllegalAccessError("No default judgement is defined yet.");

		em.close();
		return judgement;
	}

	public void updateJudgement(Judgement judgement){

		EntityManager em =  entityManagerFactory.createEntityManager();
		EntityTransaction t = em.getTransaction();
		t.begin();
		em.merge(judgement);
		t.commit();
		em.close();       
	}


	public String removeJudgement(String id) throws IllegalArgumentException{

		if (id==null)
			throw new IllegalArgumentException("String cannot be null");
		EntityManager em = entityManagerFactory.createEntityManager();
		Judgement judgement = em.find(Judgement.class,id);

		if (judgement==null)
			throw new IllegalArgumentException("String is not valid");

		em.getTransaction().begin();
		em.remove(judgement);
		em.getTransaction().commit();

		em.close();
		return id;
	}


	//-------------------------------------------------------------------------------------//
	//--------------------CaiqTree Persistence Implementation -----------------------------//
	//-------------------------------------------------------------------------------------//

	public CaiqTree retrieveCaiqTree(String id) throws IllegalArgumentException{
		EntityManager em =  entityManagerFactory.createEntityManager();

		if (id==null)
			throw new IllegalArgumentException("EvaluatedCaiq_Identifier cannot be null");
		CaiqTree caiqTree = em.find(CaiqTree.class,id);

		if (caiqTree==null)
			throw new IllegalArgumentException("EvaluatedCaiq_Identifier is not valid");

		em.close();
		return caiqTree;
	}

	public String createCaiqTree(CaiqTree caiqTree) {

		EntityManager em =  entityManagerFactory.createEntityManager();
		EntityTransaction t = em.getTransaction();
		t.begin();
		em.persist(caiqTree);
		t.commit();
		em.close();        

		return caiqTree.getId();        
	}

	public void updateCaiqTree(CaiqTree caiqTree){

		EntityManager em =  entityManagerFactory.createEntityManager();
		EntityTransaction t = em.getTransaction();
		t.begin();
		em.merge(caiqTree);
		t.commit();
		em.close();       
	}

	public String removeCaiqTree(String id) throws IllegalArgumentException{

		if (id==null)
			throw new IllegalArgumentException("String cannot be null");
		EntityManager em = entityManagerFactory.createEntityManager();
		CaiqTree caiqTree = em.find(CaiqTree.class,id);

		if (caiqTree==null)
			throw new IllegalArgumentException("String is not valid");

		em.getTransaction().begin();
		em.remove(caiqTree);
		em.getTransaction().commit();

		em.close();
		return id;
	}

	//-------------------------------------------------------------------------------------//
	//--------------------SLACaiq Persistence Implementation -----------------------------//
	//-------------------------------------------------------------------------------------//

	public String createSLACaiq(SLACaiq slaCaiq) {

		EntityManager em =  entityManagerFactory.createEntityManager();
		EntityTransaction t = em.getTransaction();
		t.begin();
		em.persist(slaCaiq);
		t.commit();
		em.close();        

		return slaCaiq.getId();      
	}

	public List<SLACaiq> retrieveSLACaiqs(int start, int stop) {
		EntityManager em =  entityManagerFactory.createEntityManager();

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<SLACaiq> cq = cb.createQuery(SLACaiq.class);

		List<SLACaiq> list;

		if(stop != -1){
			list = em.createQuery(cq).setFirstResult(start).setMaxResults(stop-start).getResultList();
		}else{
			list = em.createQuery(cq).setFirstResult(start).getResultList();
		}

		/*List<String> listId  = new ArrayList<String>();
		for (SLACaiq slaCaiq :  list){
			listId.add(new String(slaCaiq.getId()));
		}*/
		
		em.close();

		return list;

	}

	public SLACaiq retrieveSLACaiq(String id) throws IllegalArgumentException{
		EntityManager em =  entityManagerFactory.createEntityManager();

		if (id==null)
			throw new IllegalArgumentException("SlaCaiq_Identifier cannot be null");
		SLACaiq slaCaiq = em.find(SLACaiq.class,id);

		if (slaCaiq==null)
			throw new IllegalArgumentException("SlaCaiq_Identifier is not valid");

		em.close();
		return slaCaiq;
	}


}

