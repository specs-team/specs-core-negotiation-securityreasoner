package eu.specsproject.app.securityreasoner.parser;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import eu.specsproject.app.securityreasoner.entities.Caiq;

public class CaiqResourceParser {
	
	private CaiqResourceParser(){}

	public static String marshal(Caiq caiq) throws JAXBException{
		java.io.StringWriter sw = new StringWriter();

		try{
			JAXBContext jaxbContext = JAXBContext.newInstance(Caiq.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			jaxbMarshaller.marshal(caiq, sw);
		}catch(JAXBException e){
			throw e;
		}
		return sw.toString();

	}

}
