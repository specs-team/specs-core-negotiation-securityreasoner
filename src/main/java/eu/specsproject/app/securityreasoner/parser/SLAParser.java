package eu.specsproject.app.securityreasoner.parser;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import eu.specs.datamodel.agreement.offer.AgreementOffer;

public class SLAParser {

	public static AgreementOffer buildOfferFromXml(String xml) throws JAXBException {
		AgreementOffer offer = null;
		Unmarshaller unmarshaller = null;
		try {
			JAXBContext jc = JAXBContext.newInstance(AgreementOffer.class);
			unmarshaller = jc.createUnmarshaller();
		} catch (JAXBException e) {
			throw e;
		} 
		offer = (AgreementOffer) unmarshaller
				.unmarshal(new StringReader(xml));
		return offer;
	}

}
