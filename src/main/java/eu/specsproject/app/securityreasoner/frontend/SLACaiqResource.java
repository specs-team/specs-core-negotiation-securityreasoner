package eu.specsproject.app.securityreasoner.frontend;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import eu.specsproject.app.securityreasoner.entities.SLACaiq;
import eu.specsproject.app.securityreasoner.persistence.PersistenceImplementation;

public class SLACaiqResource {

	private static final Logger logger = LogManager.getLogger(SLACaiqResource.class);

	private String id;

	public SLACaiqResource (String id){
		this.id=new String(id);
	}

	@Path("/score")
	@GET
	@Produces({MediaType.TEXT_PLAIN})
	public Response score(@QueryParam("category") String category){
		PersistenceImplementation pi = new PersistenceImplementation();

		try{
			SLACaiq slacaiq = pi.retrieveSLACaiq(id);

			if(category == null){
				return Response.status(200).type("text/plain")
						.entity(slacaiq.getRootScore()).build();
			}
			else{
				try{slacaiq.getCategoryScore(category);
					return Response.status(200).type("text/plain")
							.entity(slacaiq.getCategoryScore(category)).build();
				}
				catch(IllegalArgumentException e){
					logger.error("score: client provided invalid category. " + e);
					return Response.status(404).type("text/plain")
						.entity("Not Found: Category not found.").build();
				}
			}		
		}
		catch(IllegalArgumentException e){
			logger.error("score: client provided invalid slacaiq. " + e);
			return Response.status(404).type("text/plain")
					.entity("Not Found: Sla Caiq not found.").build();
		}
	}
}
