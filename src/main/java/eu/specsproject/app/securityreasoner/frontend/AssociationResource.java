package eu.specsproject.app.securityreasoner.frontend;

import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import eu.specsproject.app.securityreasoner.entities.Caiq;
import eu.specsproject.app.securityreasoner.entities.CaiqTree;
import eu.specsproject.app.securityreasoner.persistence.PersistenceImplementation;

public class AssociationResource {

	private static final Logger logger = LogManager.getLogger(AssociationResource.class);
	private String id;

	public AssociationResource (String id){
		this.id=id;
	}

	@POST
	@Produces({MediaType.TEXT_PLAIN})
	@Consumes(MediaType.TEXT_PLAIN)
	public Response setAssociation(String idJudgement){

		PersistenceImplementation pi = new PersistenceImplementation();
		Caiq caiq = null;
		try{
			caiq = pi.retrieveCaiq(id);
		}catch(IllegalArgumentException e){
			logger.error("setAssociation: client requested non-existent caiq. " + e);
			return Response.status(404).type("text/plain")
					.entity("Error: caiq not found.").build();
		}
		if(caiq.getAssociatedTree(idJudgement) != null)			
			return Response.status(201).type("text/plain")
					.entity(idJudgement).build();
		else{
			try{
				pi.retrieveJudgement(idJudgement);
				String caiqXML = pi.retrieveCaiqDocument(caiq.getDocumentId()).getcaiqXmlDocument();
				String judgementXML = pi.retrieveJudgement(idJudgement).getJudgementXmlDocument();
				CaiqTree caiqTree = new CaiqTree();
				caiqTree.buildWeightedTree(caiqXML, judgementXML);
				String associatedTreeId = pi.createCaiqTree(caiqTree);
				caiq.addAssociatedTree(idJudgement, associatedTreeId, "weighted");
				pi.updateCaiq(caiq);
				return Response.status(201).type("text/plain")
						.entity(idJudgement).build();
			}catch(IllegalArgumentException e){
				logger.error("setAssociation: client requested non-existent judgement " + e);
				return Response.status(422).type("text/plain")
						.entity("Error: judgement not found.").build();
			} catch (IOException e) {
				logger.error("setAssociation: I/O failure " + e);
				return Response.status(500).type("text/plain")
						.entity("Server Error, try again later.").build();
			} catch (Exception e) {
				logger.error("setAssociation: internal error building the weighted tree " + e);
				return Response.status(500).type("text/plain")
						.entity("Server Error, try again later.").build();
			}
		}

	}

}
