package eu.specsproject.app.securityreasoner.frontend;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.validation.ValidationException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import edu.uci.ics.jung.graph.Tree;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceEdge;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceValueNode;
import eu.specs.negotiation.rem.representation.parsers.exceptions.NodeAlreadyPresentException;
import eu.specs.negotiation.rem.utils.FromFileToString;
import eu.specsproject.app.securityreasoner.entities.AssociatedTree;
import eu.specsproject.app.securityreasoner.entities.Caiq;
import eu.specsproject.app.securityreasoner.parser.CaiqParser;
import eu.specsproject.app.securityreasoner.parser.CaiqResourceParser;
import eu.specsproject.app.securityreasoner.parser.DocumentValidator;
import eu.specsproject.app.securityreasoner.persistence.PersistenceImplementation;
import eu.specsproject.app.securityreasoner.utility.FromInputStreamToString;
import eu.specsproject.app.securityreasoner.utility.PropertiesManager;

public class CaiqResource {

	private static final Logger logger = LogManager.getLogger(CaiqResource.class);
	private String id;

	public CaiqResource (String id){
		this.id=new String(id);
	}

	@GET
	@Produces(MediaType.APPLICATION_XML)
	public Response getCaiq(){
		PersistenceImplementation pi = new PersistenceImplementation();
		try {
			return Response.status(200).type("application/xml")
					.entity(CaiqResourceParser.marshal(pi.retrieveCaiq(id)))
					.type(MediaType.APPLICATION_XML).build();
		}catch (IllegalArgumentException e){
			logger.error("getCaiq: client requested non-existent caiq " + e);
			return Response.status(404).type("text/plain")
					.entity("Not Found: Caiq not found.").build();
		} catch (JAXBException e) {
			logger.error("getCaiq: error parsing caiq " + e);
			return Response.status(500).type("text/plain")
					.entity("Server Error, try again later.").build();
		}
	}

	@PUT
	@Consumes(MediaType.APPLICATION_XML)
	public Response updateCaiq(String newCaiqXML){

		PersistenceImplementation pi = new PersistenceImplementation();
		try{
			Caiq caiq = pi.retrieveCaiq(id);
			InputStream xsd = null;
			try {
				xsd = new FileInputStream((this.getClass().getResource("/")).toString().substring(5).replace("%20", " ") + PropertiesManager.getProperty("xsdValueTree"));
			} catch (IOException e) {
				logger.error("updateCaiq: I/O failure " + e);
				return Response.status(500).type("text/plain")
						.entity("Server Error, try again later.").build();
			}

			// Awful solution, but didn't work differently 
			try{
				DocumentValidator.validate(new ByteArrayInputStream(newCaiqXML.getBytes()), new ByteArrayInputStream(FromInputStreamToString.convert(xsd).getBytes()));

				//-----------------auto fill tree------------------------
				Tree<InterfaceValueNode, InterfaceEdge> treeIn = CaiqParser.unmarshall(newCaiqXML);

				//get leaves

				java.util.Collection<InterfaceValueNode> nodes = treeIn.getVertices();
				ArrayList<InterfaceValueNode> leaves = new ArrayList<InterfaceValueNode>();
				for (InterfaceValueNode node : nodes){
					if(treeIn.getChildCount(node) == 0){
						leaves.add(node);
					}
				}
				//retrieve caiq model tre
				FromFileToString converter = new FromFileToString();
				Tree<InterfaceValueNode, InterfaceEdge> treeModel = null;
				try {
					treeModel = CaiqParser.unmarshall(converter.convert((this.getClass().getResource("/")).toString().substring(5).replace("%20", " ") + PropertiesManager.getProperty("CaiqModel")));
				} catch (Exception e) {
					logger.error("updateCaiq: error parsing caiq " + e);
					return Response.status(500).type("text/plain")
							.entity("Server Error, try again later.").build();
				}

				//scan Model to add positive answers
				for (InterfaceValueNode node : leaves){
					boolean found = false;
					for(InterfaceValueNode nodeModel : treeModel.getVertices()){
						if(node.getName().equals(nodeModel.getName())){
							nodeModel.setValue("YES");
							found = true;
							break;
						}
					}
					//If input control is not found, the input document is not compliant with the model
					if(!found)
						return Response.status(435).type("text/plain")
								.entity("Invalid Input: request body not compliant with the defined schema.").build();
				}

				String newDocument = "";
				try {
					newDocument = CaiqParser.marshall(treeModel);
				} catch (Exception e) {
					logger.error("updateCaiq: error parsing caiq " + e);
					return Response.status(500).type("text/plain")
							.entity("Server Error, try again later.").build();
				}


				//-------------------------------------------------------


				String oldDocId = caiq.getDocumentId();
				// remove old document and  weighted/evaluated trees
				pi.removeCaiqDocument(oldDocId);
				for ( AssociatedTree association : caiq.getAssociatedTrees() ) {
					pi.removeCaiqTree(association.getIdTree());
				}
				caiq.getAssociatedTrees().clear();

				// stores new document and updates caiq
				caiq.setDocumentId(pi.storeCaiqDocument(newDocument));	
				pi.updateCaiq(caiq);						
				return Response.status(200).type("text/plain")
						.entity(caiq.getId()).build();
			}
			catch(ValidationException | SAXException e){
				logger.error("updateCaiq: client provided invalid xml. " + e);
				return Response.status(435).type("text/plain")
						.entity("Invalid Input: request body not compliant with the defined schema.").build();
			}
			catch (IOException | NodeAlreadyPresentException e1) {
				logger.error("updateCaiq: error parsing caiq. " + e1);
				return Response.status(500).type("text/plain")
						.entity("Server Error, try again later.").build();
			} catch (XMLStreamException e1) {
				logger.error("updateCaiq: error parsing caiq. " + e1);
				return Response.status(500).type("text/plain")
						.entity("Server Error, try again later.").build();
			}
		}
		catch(IllegalArgumentException e){
			logger.error("updateCaiq: client requested non-existent caiq. " + e);
			return Response.status(404).type("text/plain")
					.entity("Not Found: the specified caiq has not been found.").build();
		}
	}

	@DELETE
	@Produces(MediaType.TEXT_PLAIN)
	public Response removeCaiq(){
		PersistenceImplementation pi = new PersistenceImplementation();
		try{
			Caiq caiq = pi.retrieveCaiq(id);
			for ( AssociatedTree association : caiq.getAssociatedTrees() ) {
				pi.removeCaiqTree(association.getIdTree());
			}
			caiq.getAssociatedTrees().clear();
			pi.removeCaiqDocument(caiq.getDocumentId());
			pi.removeCaiq(id);
		}catch(IllegalArgumentException e){
			logger.error("removeCaiq: client requested non-existent caiq. " + e);
			return Response.status(404).type("text/plain")
					.entity("Not Found: the specified caiq has not been found.").build();
		}
		return Response.status(204).type("text/plain")
				.entity("No Content: the caiq has been deleted.").build();
	}

	@Path("/associate")
	public AssociationResource associateCaiqJudgement(){
		return new AssociationResource(id);
	}

	@Path("/evaluate")
	public CaiqTreeResource getEvaluatedCaiqResource(){
		return new CaiqTreeResource(id);
	}

}
