package eu.specsproject.app.securityreasoner.frontend;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import eu.specsproject.app.securityreasoner.entities.Collection;
import eu.specsproject.app.securityreasoner.entities.Item;
import eu.specsproject.app.securityreasoner.entities.Judgement;
import eu.specsproject.app.securityreasoner.parser.DocumentValidator;
import eu.specsproject.app.securityreasoner.persistence.PersistenceImplementation;
import eu.specsproject.app.securityreasoner.utility.FromInputStreamToString;
import eu.specsproject.app.securityreasoner.utility.PropertiesManager;

@Path("judgements")
public class JudgementsResource {

	private static final Logger logger = LogManager.getLogger(JudgementsResource.class);

	@Context
	UriInfo uriInfo;

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getJudgements(@QueryParam("items") Integer totalItems, @QueryParam("page") Integer page, @QueryParam("length") Integer length) {

		List<Item> items = new ArrayList<Item>();
		int start = (page != null && length != null && totalItems == null) ? page : 0; 
		int stop = (totalItems != null) ? totalItems : -1; 
		stop = (page != null && length != null && totalItems == null) ? page+length : stop; 

		PersistenceImplementation pi = new PersistenceImplementation();

		List<Judgement> judgements = pi.retrieveJudgements(start, stop);


		stop = judgements.size();
		for (int j = start; j < stop; j++){
			items.add(new Item(judgements.get(j).getId(), uriInfo.getAbsolutePath()+"/"+judgements.get(j).getId()));
		}
		Collection collection = new Collection("JUDGEMENT", judgements.size(), stop-start, items);
		GenericEntity<Collection> coll = new GenericEntity<Collection>(collection) {
		};

		return Response.ok(coll).build();

	}

	@POST
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.TEXT_PLAIN)
	public Response createJudgement(String document){
		Charset.forName("UTF-8").encode(document);
		Judgement judgement = new Judgement();
		judgement.setJudgementXmlDocument(document);

		InputStream xsd = null;
		try {
			xsd = new FileInputStream((this.getClass().getResource("/")).toString().substring(5).replace("%20", " ") + PropertiesManager.getProperty("xsdWeightsSimple"));
		}
		catch (IOException e) {
			logger.error("createJudgement: I/O failure. " + e);
			return Response.status(500).type("text/plain")
					.entity("Server Error, try again later.").build();
		}

		try{
			DocumentValidator.validate(new ByteArrayInputStream(document.getBytes()), new ByteArrayInputStream(FromInputStreamToString.convert(xsd).getBytes()));


			// If no default judgement is defined (first judgement creation) set this one as default
			PersistenceImplementation pi = new PersistenceImplementation();
			if(pi.retrieveJudgements(0, -1).isEmpty()){
				judgement.setDefaultJudgement();
			}

			String id = new PersistenceImplementation().createJudgement(judgement);

			if(pi.retrieveJudgements(0, -1).size()==1){
				System.out.println("Warning: no default judgement was set, now using " + id + " as default.");
			}

			UriBuilder ub = uriInfo.getAbsolutePathBuilder();
			URI smURI = ub.
					path(id.toString()).
					build();
			return Response.created(smURI).entity(id.toString()).build();}
		catch(IllegalArgumentException | SAXException e){
			logger.error("createJudgement: client provided invalid xml. " + e);
			return Response.status(435).type("text/plain")
					.entity("Invalid Input: request body not compliant with the defined schema.").build();
		}
		catch (IOException e) {
			logger.error("createJudgement: I/O failure. " + e);
			return Response.status(500).type("text/plain")
					.entity("Server Error, try again later.").build();
		}
	}

	@Path("/{id}")
	public JudgementResource getJudgement(@PathParam("id") String id) {
		return new JudgementResource(id);
	}

}
