package eu.specsproject.app.securityreasoner.frontend;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.uci.ics.jung.graph.Tree;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceRequirementNode;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceWeightedEdge;
import eu.specs.negotiation.rem.representation.data_structures.exceptions.SlNotSetException;
import eu.specsproject.app.securityreasoner.entities.Caiq;
import eu.specsproject.app.securityreasoner.entities.CaiqTree;
import eu.specsproject.app.securityreasoner.entities.CategoryScore;
import eu.specsproject.app.securityreasoner.parser.CaiqTreeParser;
import eu.specsproject.app.securityreasoner.persistence.PersistenceImplementation;

public class CaiqTreeResource {

	private static final Logger logger = LogManager.getLogger(CaiqTreeResource.class);

	@Context
	UriInfo uriInfo;

	private String caiqId;

	public CaiqTreeResource (String id){
		this.caiqId=id;
	}

	@GET
	@Produces({MediaType.APPLICATION_XML })
	public Response evaluate(@Context UriInfo ui){

		MultivaluedMap<String, String> queryParams = ui.getQueryParameters();
		String idJudgement = queryParams.getFirst("idJudgement");

		// If no argument is given to the call, evaluate the caiq with the default judgement
		if(idJudgement == null){
			try {
				idJudgement = new PersistenceImplementation().retrieveDefaultJudgement().getId();
			} catch (IllegalAccessError e) {
				logger.error("evaluate: there's no default judgement set. " + e);
				return Response.status(500).type("text/plain")
						.entity("Server Error, try again later.").build();
			}
		}

		PersistenceImplementation pi = new PersistenceImplementation();
		Caiq caiq = pi.retrieveCaiq(caiqId);
		if(!caiq.isAssociated(idJudgement))
			return Response.status(409).type("text/plain")
					.entity("Conflict: caiq not associated to this judgement.").build();
		else{		
			CaiqTree caiqTree = pi.retrieveCaiqTree(caiq.getAssociatedTree(idJudgement).getIdTree());
			// If state equals "weighted", evaluate the caiq...
			if("weighted".equals(caiq.getAssociatedTree(idJudgement).getState())){
				try {
					caiqTree.evaluate();
				} catch (Exception e) {
					logger.error("evaluate: error during calculations. " + e);
					return Response.status(500).type("text/plain")
							.entity("Server Error, try again later.").build();
				}

				// Update stored info
				pi.updateCaiqTree(caiqTree);
				caiq.getAssociatedTree(idJudgement).setState("evaluated");
				pi.updateCaiq(caiq);
			}
			// ...Otherwise do nothing, anyway return its id
			/*return Response.status(201).type("text/xml")
					.entity(caiqTree.getCaiqTreeXMLDocument()).build();*/

			//invece di restituire l'intero xml restituisce lo score di root, oppure quello della categoria specificata
			String category = queryParams.getFirst("category");

			Tree<InterfaceRequirementNode, InterfaceWeightedEdge> scoreTree = null;
			try {
				scoreTree = CaiqTreeParser.unmarshall(caiqTree.getCaiqTreeXMLDocument());
			} catch (Exception e) {
				logger.error("evaluate: error during calculations. " + e);
				return Response.status(500).type("text/plain")
						.entity("Server Error, try again later.").build();
			}

			if(category == null){
				try {
					return Response.status(201).type("text/xml")
							.entity(Double.toString(scoreTree.getRoot().getSl())).build();
				} catch (SlNotSetException e) {
					logger.error("evaluate: error during calculations. " + e);
					return Response.status(500).type("text/plain")
							.entity("Server Error, try again later.").build();
				}
			}
			else{
				try{
					return Response.status(200).type("text/plain")
							.entity(Double.toString(getCategoryScore(scoreTree, category))).build();
				}
				catch(IllegalArgumentException e){
					logger.error("score: client provided invalid category. " + e);
					return Response.status(404).type("text/plain")
							.entity("Not Found: Category not found.").build();
				} catch (SlNotSetException e) {
					logger.error("adjustCaiq: internal error. " + e);
					return Response.status(500).type("text/plain")
							.entity("Server Error, try again later.").build();
				}

			}

		}
	}

	private double getCategoryScore(Tree<InterfaceRequirementNode, InterfaceWeightedEdge> scoreTree, String category) throws IllegalArgumentException, SlNotSetException{

		ArrayList<CategoryScore> scoreCategories = new ArrayList<CategoryScore>();
		java.util.Collection<InterfaceRequirementNode> collection = scoreTree.getChildren(scoreTree.getRoot());
		for (InterfaceRequirementNode node : collection){
			try {
				scoreCategories.add(new CategoryScore (node.getName(), node.getSl()));
			} catch (SlNotSetException e) {
				throw e;
			}
		}
		for(CategoryScore c : scoreCategories){
			if(c.getCategory() != null && c.getCategory().equals(category)){
				return c.getScore();
			}
		}
		throw new IllegalArgumentException();
	}


}
