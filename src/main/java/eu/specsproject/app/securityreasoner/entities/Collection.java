package eu.specsproject.app.securityreasoner.entities;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class Collection {

	    @XmlAttribute
	    public String resource;
	    
	    @XmlAttribute
	    public int total;
	    
	    @XmlAttribute
	    public int members;
	    
	    @XmlElement
	    public List<Item> item;
	    
	    public Collection(){
	        //Zero arguments constructor
	    }
	    
	    public Collection(String resource, int total, int members, List<Item> item){
	        this.resource=resource;
	        this.total=total;
	        this.members=members;
	        this.item = item;
	    }
	
}
