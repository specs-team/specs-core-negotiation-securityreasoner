package eu.specsproject.app.securityreasoner.entities;

import java.io.IOException;
import java.io.Serializable;

import edu.uci.ics.jung.graph.Tree;
import eu.specs.negotiation.rem.elaboration.tree_rem_evaluators.impl.AverageSumTreeRemEvaluator;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceRequirementNode;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceWeightedEdge;
import eu.specsproject.app.securityreasoner.utility.FromFileToString;
import eu.specsproject.app.securityreasoner.evaluation.ExecTreeSimpleWeightsInstantiator;
import eu.specsproject.app.securityreasoner.evaluation.ExecTreeValueNormClust;
import eu.specsproject.app.securityreasoner.parser.CaiqTreeParser;
import eu.specsproject.app.securityreasoner.utility.PropertiesManager;

public class CaiqTree implements Serializable{

	private static final long serialVersionUID = 1075385464218712038L;
	String id;
	String caiqTreeXMLDocument;

	public void buildWeightedTree(String caiqXML, String judgementXML) throws Exception {

		String xmlRelationTree=null;
		String xsdValueTree=null;
		String xsdRelationTree = null;
		String xsdWeightsSimple = null;

		FromFileToString converter = new FromFileToString();

		try {

			//schema path execTreeValueNormClust
			String path = (this.getClass().getResource("/")).toString().substring(5).replace("%20", " ");
			xsdValueTree = converter.convert(path + PropertiesManager.getProperty("xsdValueTree"));
			xsdRelationTree = converter.convert(path + PropertiesManager.getProperty("xsdRelationTree"));

			//schema path execTreeSimpleWeights
			xsdWeightsSimple = converter.convert(path + PropertiesManager.getProperty("xsdWeightsSimple"));

			//xml path execTreeValueNormClust
			xmlRelationTree = converter.convert(path + PropertiesManager.getProperty("inputXmlRelationTreePath"));

		} catch (IOException e) {
			throw e;
		}

		// clustering
		ExecTreeValueNormClust clust = new ExecTreeValueNormClust();
		Tree<InterfaceRequirementNode, InterfaceWeightedEdge> treeClustered;
		try {
			treeClustered = clust.exec(caiqXML,xsdValueTree, xmlRelationTree,xsdRelationTree);
		} catch (Exception e) {
			throw e;
		}

		// weighting
		Tree<InterfaceRequirementNode, InterfaceWeightedEdge> treeWeighted;
		ExecTreeSimpleWeightsInstantiator weight = new ExecTreeSimpleWeightsInstantiator();	
		try {
			treeWeighted = weight.exec(treeClustered,judgementXML,xsdWeightsSimple);
		} catch (Exception e) {
			throw e;
		}

		this.caiqTreeXMLDocument = CaiqTreeParser.marshall(treeWeighted);

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCaiqTreeXMLDocument() {
		return caiqTreeXMLDocument;
	}

	public void setCaiqTreeXMLDocument(String caiqTreeXMLDocument) {
		this.caiqTreeXMLDocument = caiqTreeXMLDocument;
	}

	public void evaluate() throws Exception{

		Tree<InterfaceRequirementNode, InterfaceWeightedEdge> treeWeighted;
		try {
			treeWeighted = CaiqTreeParser.unmarshall(this.getCaiqTreeXMLDocument());
		} catch (Exception e) {
			throw e;
		}

		AverageSumTreeRemEvaluator evaluator = new AverageSumTreeRemEvaluator();
		Tree<InterfaceRequirementNode, InterfaceWeightedEdge> tree_out = evaluator.evaluate(treeWeighted);

		try {
			setCaiqTreeXMLDocument(CaiqTreeParser.marshall(tree_out));
		} catch (Exception e) {
			throw e;
		}

	};

}
