package eu.specsproject.app.securityreasoner.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.uci.ics.jung.graph.Tree;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceEdge;
import eu.specs.negotiation.rem.representation.data_structures.InterfaceValueNode;
import eu.specsproject.app.securityreasoner.entities.AssociatedTree;
import eu.specsproject.app.securityreasoner.entities.Caiq;
import eu.specsproject.app.securityreasoner.entities.CaiqTree;
import eu.specsproject.app.securityreasoner.entities.Judgement;
import eu.specsproject.app.securityreasoner.parser.CaiqParser;
import eu.specsproject.app.securityreasoner.persistence.PersistenceImplementation;
import eu.specsproject.app.securityreasoner.utility.ApplicationConfig;
import org.junit.Assert;


public class SecurityReasonerTest extends JerseyTest{

	private static String caiqId;
	private static String judgementDefId;
	private static String judgementId;



	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		//SetUp environment
		PersistenceImplementation.isTest = true;
		PersistenceImplementation pi = new PersistenceImplementation();

		//create caiq

		//-----------------auto fill tree------------------------
		Tree<InterfaceValueNode, InterfaceEdge> treeIn = CaiqParser.unmarshall(readFile("Amazon-EC2-CAIQ-CCM-3.0.xml"));

		//get leaves

		java.util.Collection<InterfaceValueNode> nodes = treeIn.getVertices();
		ArrayList<InterfaceValueNode> leaves = new ArrayList<InterfaceValueNode>();
		for (InterfaceValueNode node : nodes){
			if(treeIn.getChildCount(node) == 0){
				leaves.add(node);
			}
		}
		//retrieve caiq model tree
		Tree<InterfaceValueNode, InterfaceEdge> treeModel = null;
		try {
			treeModel = CaiqParser.unmarshall(readFile("caiqModel.xml"));
		} catch (Exception e) {
			System.out.println("System Error: Cannot find caiq model");
			e.printStackTrace();
		}

		//scan Model to add positive answers
		for (InterfaceValueNode node : leaves){
			for(InterfaceValueNode nodeModel : treeModel.getVertices()){
				if(node.getName().equals(nodeModel.getName())){
					nodeModel.setValue("YES");
					break;
				}
			}
		}

		String newDocument = "";
		try {
			newDocument = CaiqParser.marshall(treeModel);
		} catch (Exception e) {
			e.printStackTrace();
		}


		//-------------------------------------------------------

		caiqId = pi.createCaiq("Amazon-EC2", newDocument);

		//Create two judgement, one default and one not
		Judgement judgement = new Judgement();
		judgement.setJudgementXmlDocument(readFile("judgement1.xml"));
		judgement.setDefaultJudgement();
		judgementDefId = pi.createJudgement(judgement);

		Judgement judgement2 = new Judgement();
		judgement2.setJudgementXmlDocument(readFile("judgement1.xml"));
		judgementId = pi.createJudgement(judgement2);

		//Associate the non-default judgement to the caiq
		Caiq caiq = pi.retrieveCaiq(caiqId);
		CaiqTree caiqTree = new CaiqTree();
		caiqTree.buildWeightedTree(pi.retrieveCaiqDocument(caiq.getDocumentId()).getcaiqXmlDocument(), pi.retrieveJudgement(judgementId).getJudgementXmlDocument());
		String associatedTreeId = pi.createCaiqTree(caiqTree);
		caiq.addAssociatedTree(judgementId, associatedTreeId, "weighted");
		pi.updateCaiq(caiq);

		System.out.println("setUpBeforeClass Called");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {

		PersistenceImplementation pi = new PersistenceImplementation();

		try{
			Caiq caiq = pi.retrieveCaiq(caiqId);		
			for ( AssociatedTree association : caiq.getAssociatedTrees() ) {
				pi.removeCaiqTree(association.getIdTree());
			}
			caiq.getAssociatedTrees().clear();
			pi.removeCaiqDocument(caiq.getDocumentId());
			pi.removeCaiq(caiqId);
		}catch(IllegalArgumentException e){

		}

		pi.removeJudgement(judgementId);
		pi.removeJudgement(judgementDefId);

		System.out.println("tearDownAfterClass Called");
	}

	@Before
	public void setUpChild() throws Exception {
		System.out.println("setUp Called");
	}

	@After
	public void tearDownChild() throws Exception {
		System.out.println("tearDown Called");
	}

	@Override
	public Application configure() {
		return new ApplicationConfig();
	}

	@Override
	protected void configureClient(ClientConfig config){
		config.register(MultiPartFeature.class);
	}

	@Test
	public final void retrieveCaiqsTest() {        

		Response response = target("/caiqs").request().get(Response.class);
		Assert.assertEquals(200, response.getStatus());

	}

	@Test
	public final void createCaiqTest() {        

		Response response = createCaiq("input/Amazon-EC2-CAIQ-CCM-3.0.xml");
		Assert.assertEquals(201, response.getStatus());

		Response response2 = createCaiq("input/WrongControl-CAIQ.xml");
		Assert.assertEquals(435, response2.getStatus());

		Response response3 = createCaiq("input/WrongFormat-CAIQ.xml");
		Assert.assertEquals(435, response3.getStatus());

	}

	@Test
	public final void retrieveCaiqByIdTest() {        

		Response response = target("/caiqs/" + caiqId).request().get(Response.class);
		Assert.assertEquals(200, response.getStatus());

		Response response2 = target("/caiqs/" + "abc").request().get(Response.class);
		Assert.assertEquals(404, response2.getStatus());

	}

	@Test
	public final void deleteCaiqTest() {    

		Response response = target("/caiqs/" + caiqId).request().delete();
		Assert.assertEquals(204, response.getStatus());

		Response response2 = target("/caiqs/" + "abc").request().delete();
		Assert.assertEquals(404, response2.getStatus());

	}

	@Test
	public final void updateCaiqByIdTest() {        

		String caiq = "";
		try {
			caiq = readFile("input/Amazon-EC2-CAIQ-CCM-3.0.xml");
		} catch (IOException e) {
			e.printStackTrace();
		}

		Response response = target("/caiqs/" + caiqId).request().put(Entity.entity(caiq, MediaType.APPLICATION_XML));
		Assert.assertEquals(200, response.getStatus());

		String caiq2 = "";
		try {
			caiq2 = readFile("input/WrongControl-CAIQ.xml");
		} catch (IOException e) {
			e.printStackTrace();
		}

		Response response2 = target("/caiqs/" + caiqId).request().put(Entity.entity(caiq2, MediaType.APPLICATION_XML));
		Assert.assertEquals(435, response2.getStatus());

	}

	@Test
	public final void associateCaiqToJudgementTest() {

		Response response = target("/caiqs/" + caiqId + "/associate").request().post(Entity.entity(judgementId, MediaType.TEXT_PLAIN));
		Assert.assertEquals(201, response.getStatus());

		Response response2 = target("/caiqs/" + caiqId + "/associate").request().post(Entity.entity(judgementDefId, MediaType.TEXT_PLAIN));
		Assert.assertEquals(201, response2.getStatus());

		Response response3 = target("/caiqs/" + "abc" + "/associate").request().post(Entity.entity(judgementId, MediaType.TEXT_PLAIN));
		Assert.assertEquals(404, response3.getStatus());

		Response response4 = target("/caiqs/" + caiqId + "/associate").request().post(Entity.entity("abc", MediaType.TEXT_PLAIN));
		Assert.assertEquals(422, response4.getStatus());

	}

	@Test
	public final void evaluateCaiqTest() {

		Response response = target("/caiqs/" + caiqId + "/evaluate").queryParam("idJudgement", judgementId).request().get();
		Assert.assertEquals(201, response.getStatus());

		Response response2 = target("/caiqs/" + caiqId + "/evaluate").request().get();
		Assert.assertEquals(409, response2.getStatus());

	}

	@Test
	public final void retrieveJudgementsTest() {        
		Response response = target("/judgements").request().get(Response.class);
		Assert.assertEquals(200, response.getStatus());
	}


	@Test
	public final void createJudgementTest() { 
		String judgement = null;

		try {
			judgement = readFile("judgement1.xml");
		} catch (IOException e) {
			e.printStackTrace();
		}

		Response response = target("/judgements").request().post(Entity.entity(judgement, MediaType.APPLICATION_XML));
		Assert.assertEquals(201, response.getStatus());
	}
	
	@Test
	public final void retrieveJudgementTest() { 

		Response response = target("/judgements/" + judgementId).request().get(Response.class);
		Assert.assertEquals(200, response.getStatus());
	}
	
	@Test
	public final void updateJudgementTest() { 
		
		String judgement = "";
		try {
			judgement = readFile("judgement1.xml");
		} catch (IOException e) {
			e.printStackTrace();
		}

		Response response = target("/judgements/" + judgementId).request().put(Entity.entity(judgement, MediaType.APPLICATION_XML));
		Assert.assertEquals(409, response.getStatus());
		
		Response response2 = target("/judgements/" + judgementDefId).request().put(Entity.entity(judgement, MediaType.APPLICATION_XML));
		Assert.assertEquals(409, response2.getStatus());
	}

	@Test
	public final void createSlaCaiqtest(){
		String slaCaiq = null;

		try {
			slaCaiq = readFile("input/specs-SLATemplate-SecureStorage_M30_CCM_Amazon.xml");
		} catch (IOException e) {
			e.printStackTrace();
		}

		Response response = target("/slacaiqs").request().post(Entity.entity(slaCaiq, MediaType.APPLICATION_XML));
		Assert.assertEquals(201, response.getStatus());
	}

	@Test
	public final void retrieveSlaCaiqsTest() {        
		Response response = target("/slacaiqs").request().get(Response.class);
		Assert.assertEquals(200, response.getStatus());
	}


	@Test
	public final void retrieveSlaCaiqScoreByIdTest(){

		String slaCaiq = null;
		try {
			slaCaiq = readFile("input/specs-SLATemplate-SecureStorage_M30_CCM_Amazon.xml");
		} catch (IOException e) {
			e.printStackTrace();
		}
		Response response = target("/slacaiqs").request().post(Entity.entity(slaCaiq, MediaType.APPLICATION_XML));
		Assert.assertEquals(201, response.getStatus());
		
		String slacaiqId = response.readEntity(String.class);

		Response response1 = target("/slacaiqs/" + slacaiqId + "/score").request().get(Response.class);
		Assert.assertEquals(200, response1.getStatus());
		
		Response response1_ = target("/slacaiqs/" + slacaiqId + "/score").queryParam("category", "EKM").request().get(Response.class);
		Assert.assertEquals(200, response1_.getStatus());
		
		Response response1__ = target("/slacaiqs/" + slacaiqId + "/score").queryParam("category", "ELM").request().get(Response.class);
		Assert.assertEquals(404, response1__.getStatus());


		Response response2 = target("/caiqs/" + "abc").request().get(Response.class);
		Assert.assertEquals(404, response2.getStatus());
	}

	private Response createCaiq(String fileName){

		final FormDataMultiPart formDataMultiPart = new FormDataMultiPart();
		String value = null;
		try {
			value = readFile(fileName);
		} catch (IOException e) {
			e.printStackTrace();
		}
		final FormDataContentDisposition dispo = FormDataContentDisposition//
				.name("file")//
				.fileName("input/Amazon-EC2-CAIQ-CCM-3.0.xml")//
				.size(value.getBytes().length)//
				.build();
		final FormDataBodyPart bodyPart = new FormDataBodyPart(dispo, value);
		formDataMultiPart.bodyPart(bodyPart);

		Response response = target("/caiqs").request()
				.post(Entity.entity(formDataMultiPart, formDataMultiPart.getMediaType()));

		return response;
	}

	public static String readFile(String fileName) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(SecurityReasonerTest.class.getClassLoader().getResourceAsStream(fileName),"UTF8"));
		String tempValue="";
		String line;
		while((line = br.readLine()) != null){
			tempValue +=(line+"\n");
		}
		br.close();
		return tempValue;

	}
}
